class X {
	
	static {
		System.loadLibrary("freelan");
	}
	
	public native void run(const Jcli_configuration configuration, int exit_signal);
	public native bool parse_options(jint argc, jstring argv, jcli_configuration configuration)
}