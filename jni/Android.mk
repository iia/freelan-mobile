LOCAL_PATH := $(call my-dir)
#Vider les variables utilisées
include $(CLEAR_VARS)

TOP_PATH := /home/loic/workspace/FreeLanMobile/jni
NDK_ROOT := /home/loic/Application/adt-bundle-linux-x86_64-20131030/ndk

include $(LOCAL_PATH)/boost-1_53_0/lib/boost.mk 

#Nom du module (donc de la bibliothéque générée)
LOCAL_CFLAGS += -I$(LOCAL_PATH)/boost-1_53_0/include/boost-1_53
LOCAL_LDLIBS += -L$(LOCAL_PATH)/boost-1_53_0/lib/-lboost_system

LOCAL_CPPFLAGS += -frtti
LOCAL_MODULE := freelan

#Fichiers sources utilisées
LOCAL_SRC_FILES := /home/loic/workspace/FreeLanMobile/jni/X.c
#Type de fichier en sortie (ici bibliothéque partagée)
include $(BUILD_SHARED_LIBRARY)
